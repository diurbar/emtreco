////////////////////////////////////////////////////////////////////////
// Class:        MatchingWithTwoCRTsSlope
// Module Type:  analyzer
// File:         MatchingWithTwoCRTs_module.h
// Author:       Arbin Timilsina, arbint@bnl.gov //Edited by Joseph Previdi, jprevidi@fnal.gov
////////////////////////////////////////////////////////////////////////

#ifndef MatchingWithTwoCRTs_Module_H
#define MatchingWithTwoCRTs_Module_H

// Framework includes
#include "art/Framework/Core/EDAnalyzer.h"

#include "larcore/Geometry/Geometry.h"

// ROOT includes
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TImage.h"

const double RAD_TO_DEG = 180.0 / M_PI;
const double CRT_RESOLUTION = 2.5; //cm
const double VARIANCE = 9; //cm

// Angle binning
const int BINS_ANGLE = 180;
const double MIN_BINS_ANGLE = 0.0;
const double MAX_BINS_ANGLE = 180.0;

// Selection cuts
const double MIN_TRACK_LENGTH_YZ = 40.0;
const double MIN_TRACK_ENERGY = 0.10;

const double MAX_DELTA_SLOPE_YZ = 0.1;
const double MAX_DELTA_Y = 25.0;
const double MAX_DELTA_SLOPE_XZ = 0.1;
const double MIN_DELTA_X = 35.0;

typedef struct
{
    int tempId;

    double hitPositionX;
    double hitPositionY;
    double hitPositionZ;

    int primaryTrackId;
    double primaryEnergy;
    double primaryAngleYZ;
    double primaryAngleXZ;
} hits;

typedef struct
{
    std::pair<int, int> tempId;

    std::pair<double, double> hitPositionX;
    std::pair<double, double> hitPositionY;
    std::pair<double, double> hitPositionZ;

    std::pair<int, int> primaryTrackId;
    std::pair<double, double> primaryEnergy;
    std::pair<double, double> primaryAngleYZ;
    std::pair<double, double> primaryAngleXZ;

    bool combinatorialTrack_FB;
    bool combinatorialTrack_FT;
    bool combinatorialTrack_BT;
} combinatorialTracks;

typedef struct
{
    std::pair<int, int> tempId;
    std::pair<int, int> index;
    std::pair<double, double> slopeYZ;
    std::pair<double, double> slopeXZ;

    int trackId[3];
    double energy[3];

    double deltaX;
    double directionCosine;
    double distanceOfClosestApproachYZ;
    double chiSquaredYZ;
    double averageDistance;
    double averageSignedDistance;
} tracksPair;

struct sortPair
{
    bool operator()(const tracksPair &pair1, const tracksPair &pair2)
    {
	return (pair1.deltaX < pair2.deltaX);
    }
};

struct removePairIndex
{
    const tracksPair tracksPair1;
removePairIndex(const tracksPair &tracksPair0)
: tracksPair1(tracksPair0)
    {}

    bool operator() (const tracksPair &tracksPair2)
    {
        return (tracksPair1.index.first == tracksPair2.index.first ||
                tracksPair1.tempId.first == tracksPair2.tempId.first ||
                tracksPair1.tempId.second == tracksPair2.tempId.second);
    }
};
/////////////////////////////////////////////////////////////
//Uses formula for point-to-line distance to return the 
//distance from the line between the points 
//(firstPoint1,firstPoint2) and (secondPoint1, secondPoint2) 
//to the point given by the Y-Z coordinates of the TVector3 
//"point". 
/////////////////////////////////////////////////////////////
double signedPointToLineDistance(double firstPoint1, double firstPoint2, double secondPoint1, double secondPoint2, TVector3 point){
        double numerator = (secondPoint2-firstPoint2)*point.Y() - (secondPoint1-firstPoint1) * point.Z() + secondPoint1*firstPoint2 - firstPoint1*secondPoint2; //removed the absolute value here, so will give signed distance //the sign indicates a right-hand ruled sign: cross product of line-to-point vector and the direction of the vector (in that order) gives the sign of the result
        double denominator = sqrt( (secondPoint2-firstPoint2)*(secondPoint2-firstPoint2) + (secondPoint1-firstPoint1)*(secondPoint1-firstPoint1) );

        return numerator/denominator;

}

namespace CRTMatching
{
    class MatchingWithTwoCRTsSlope : public art::EDAnalyzer
	{
	public:
	    /////////////////////////////////////////////////////////
	    // Constructor and destructor
	    /////////////////////////////////////////////////////////
	    explicit MatchingWithTwoCRTsSlope(fhicl::ParameterSet const& pset);
	    virtual ~MatchingWithTwoCRTsSlope();

	    /////////////////////////////////////////////////////////
	    //Required functions
	    /////////////////////////////////////////////////////////
	    //Called once per event
	    void analyze(const art::Event& evt);

	    /////////////////////////////////////////////////////////
	    //Selected optional functions
	    /////////////////////////////////////////////////////////
	    // Called once at the beginning of the job
	    void beginJob();

	    // Called once at the end of the job
	    void endJob();

	    // Called in to read .fcl file parameters
	    void reconfigure(fhicl::ParameterSet const& pset);

	    double setAngle(double angle)
	    {
		if(angle < 0)
		    {
			angle += M_PI;
		    }
		angle *= RAD_TO_DEG;
		return angle;
	    }

	private:

	    // Log file
	    std::ofstream logFile;

	    // The parameters that will be read from the .fcl file
	    std::string fTrackModuleLabel;
	    int fSelectedPDG;

	    // Muon Counter geometry
	    geo::MuonCounter35Alg *geoMuonCounter;
	    std::vector< std::vector<double> > counterGeometry;

	    //BackTracker
	    art::ServiceHandle<cheat::BackTracker> backTracker;

	    // Pointers to services
	    art::ServiceHandle<geo::Geometry> fGeom;

	    //For statistics
	    unsigned int nEvents, nPrimaryMuons, nPrimaryMuonsWithTwoHits;
	    unsigned int nPrimaryHits_F, nPrimaryHits_B, nPrimaryHits_T;
	    unsigned int nPrimaryHits_FB, nPrimaryHits_FT, nPrimaryHits_BT;
	    unsigned int nCombinatorialTracks_FB, nCombinatorialTracks_FT, nCombinatorialTracks_BT;
	    unsigned int nTotalRecoTracks, nConsideredRecoTracks, nPrimaryMatchedRecoTracks;
	    unsigned int nAllCRTMatchedRecoTracks, nGoodCRTMatchedRecoTracks;
	    unsigned int totalSize;
	    int toBeSubtracted;

 
	    double trueMean[6]= { };
	    double totalChiSquared; 

	    //For hits
	    std::vector<hits> primaryHits_F;
	    std::vector<hits> primaryHits_B;
	    std::vector<hits> primaryHits_T;

	    bool verbo = false;

	    //Histograms
	    TH1D *hStatistics;

	    //////////////////////////////////////////
	    //For sanity check
	    TH1D *hPrimaryAngleYZ_FB;
	    TH1D *hPrimaryAngleYZ_FT;
	    TH1D *hPrimaryAngleYZ_BT;
	    TH1D *hPrimaryAngleXZ_FB;
	    TH1D *hPrimaryAngleXZ_FT;
	    TH1D *hPrimaryAngleXZ_BT;

	    TH1D *hCombinatorialAngleYZ_FB;
	    TH1D *hCombinatorialAngleYZ_FT;
	    TH1D *hCombinatorialAngleYZ_BT;
	    TH1D *hCombinatorialAngleXZ_FB;
	    TH1D *hCombinatorialAngleXZ_FT;
	    TH1D *hCombinatorialAngleXZ_BT;

	    TH1D *hCombinatorialPrimaryAngleYZ_FB;
	    TH1D *hCombinatorialPrimaryAngleYZ_FT;
	    TH1D *hCombinatorialPrimaryAngleYZ_BT;
	    TH1D *hCombinatorialPrimaryAngleXZ_FB;
	    TH1D *hCombinatorialPrimaryAngleXZ_FT;
	    TH1D *hCombinatorialPrimaryAngleXZ_BT;
	    //////////////////////////////////////////

	    TH1D *hRecoTrackLengthYZ;
	    TH2D *hRecoTrackLengthYZVsEnergy;

	    TH1D *hDeltaSlopeYZ;
	    TH1D *hDeltaSlopeXZ;

	    TH1D *hDeltaY1;
	    TH1D *hDeltaY2;
	    TH1D *hDeltaX;

	    TH1D *hConsideredRecoTrack_RecoEnergy;
	    TH1D *hPrimaryMatchedRecoTrack_RecoEnergy;
	    TH1D *hPrimaryMatchedRecoTrack_TrueEnergy;
	    TH1D *hAllCRTMatchedRecoTrack_RecoEnergy;
	    TH1D *hAllCRTMatchedRecoTrack_TrueEnergy;
	    TH1D *hGoodCRTMatchedRecoTrack_RecoEnergy;
	    TH1D *hGoodCRTMatchedRecoTrack_TrueEnergy;

	    TH1D *hDirectionCosine; //JP added direction cosine histogram
	    TH1D *hDirectionCosineBeforeDeltaX;
	     
   	    TH1D *hDistanceOfClosestApproachYZ; //JP added distance of closest approach histogram
	    TH1D *hDistanceOfClosestApproachYZBeforeDeltaX;
	    TH1D *hSignedDistanceOfClosestApproachYZ;
	    TH1D *hSignedDistanceOfClosestApproachYZBeforeDeltaX;
	    TH1D *hChiSquaredYZ;
	    TH1D *haverageDistance;
	    TH1D *haverageSignedDistance; 
	}; // class MatchingWithTwoCRTsSlope

	void createPNG(TH1D * histo){
		//save important histograms as PNG
		TCanvas *c = new TCanvas;
		TH1D *h = histo;
		h->Draw();
		TImage *img = TImage::Create();
		img->FromPad(c);
		img->WriteImage( ( std::string(histo->GetName()) + ".png").c_str() );
		delete h;
		delete c;
		delete img;
		
	}

}// namespace CRTMatching

DEFINE_ART_MODULE(CRTMatching::MatchingWithTwoCRTsSlope)

#endif // MatchingWithTwoCRTs_Module

